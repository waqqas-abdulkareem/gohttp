package handlers

import(
	"net/http"
	"encoding/base64"
	"strings"
)

type BasicAuthHandler struct{}
type Authenticator func(username,password string) bool
type Handler func(w http.ResponseWriter, request * http.Request)

func (self BasicAuthHandler) Get(authenticator Authenticator, handler Handler) Handler{

	return func(w http.ResponseWriter, request * http.Request){

		if request.Method != "GET" {
			http.Error(w,http.StatusText(http.StatusMethodNotAllowed),http.StatusMethodNotAllowed)
			return;
		}

		username, password, authenticationProvided := basicAuth(request)


		if authenticationProvided && authenticator(username, password) {
			handler(w,request)
			
		}else{
			http.Error(w,http.StatusText(http.StatusUnauthorized),http.StatusUnauthorized);
		}

	}

	
}

//The following code was copied from go/http/Request.go source 
//because Google App Engine used Go v1.2 at the time that this 
//utility was being developed and this version does not define
//Request.BasicAuth()


// BasicAuth returns the username and password provided in the request's
// Authorization header, if the request uses HTTP Basic Authentication.
// See RFC 2617, Section 2.
func basicAuth(r *http.Request) (username, password string, ok bool) {
	auth := r.Header.Get("Authorization")
	if auth == "" {
		return
	}
	return parseBasicAuth(auth)
}

// parseBasicAuth parses an HTTP Basic Authentication string.
// "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==" returns ("Aladdin", "open sesame", true).
func parseBasicAuth(auth string) (username, password string, ok bool) {
	if !strings.HasPrefix(auth, "Basic ") {
		return
	}
	c, err := base64.StdEncoding.DecodeString(strings.TrimPrefix(auth, "Basic "))
	if err != nil {
		return
	}
	cs := string(c)
	s := strings.IndexByte(cs, ':')
	if s < 0 {
		return
	}
	return cs[:s], cs[s+1:], true
}