package gohttp

import (
	"bitbucket.org/waqqas-abdulkareem/gohttp/ghttp"
	"bitbucket.org/waqqas-abdulkareem/gohttp/cache"
	"bitbucket.org/waqqas-abdulkareem/gohttp/inflight"
	_ "errors"
	"fmt"
	"log"
	"net/http"
	"time"
)

//not used
const DefaultTimeout time.Duration = time.Minute

type Dispatcher struct {
	
	http.Client

	queue []*ghttp.Request

	cache *cache.Cache

	inflight *inflight.Inflight

	RequestChannel chan *ghttp.Request

	quitChannel			chan struct{}

	requestCompletedBroadcaster chan string

	RequestStrategy func(dispatcher *Dispatcher, request *ghttp.Request) (*http.Response, error)

	Log func(format string, a ...interface{})
}

//Returns a reference to a new instance of a request queue that used the default configurations.
//Set custom configurations before calling HandleRequestAsync(), preferably in the init function.
func NewDispatcher() *Dispatcher {

	d := &Dispatcher{
		queue:						 make([]*ghttp.Request,0),
		cache:					     cache.New(),
		inflight:					 inflight.New(),
		RequestChannel:              make(chan *ghttp.Request, 1),
		quitChannel:				 make(chan struct{},1),
		requestCompletedBroadcaster: make(chan string, 1),
		RequestStrategy:             RequestStrategy,
		Log:                         Log,
	}

	d.Timeout = DefaultTimeout

	return d
}

func (dispatch *Dispatcher) Run() {

	go dispatch.cache.Run()
	go dispatch.inflight.Run()
	go dispatch.wait()

	for {
		select {
		case requestRef := <-dispatch.RequestChannel:

			//create local copy of request. This will be used internally.
			var localReq ghttp.Request = *requestRef

			dispatch.Log("A Request has been recevied: '%v'", localReq.URL.String())

			if dispatch.inflight.Find(&localReq){
				dispatch.Log("Already in flight")
				dispatch.queue = append(dispatch.queue,&localReq)
				continue
			}

			if resp,found := dispatch.cache.Find(&localReq);found {
				
				localReq.ResponseChannel <- resp
				continue
			}

			go dispatch.doNetworkRequest(&localReq)

		case <-dispatch.quitChannel:
			
			return

		}

	}
}

func (dispatch * Dispatcher) Close() {
	dispatch.quitChannel <- struct{}{}
}

func (dispatch *Dispatcher) doNetworkRequest(localReq *ghttp.Request) {

		dispatch.Log("Will perform network request for %v.",localReq.URL.String())
		
		//Add Request to queue
		dispatch.inflight.Add(localReq)

		resp, err := dispatch.RequestStrategy(dispatch, localReq)

		if err != nil {

			localReq.Err = err
			localReq.ResponseChannel <- nil

		} else {
			response, err := ghttp.NewResponse(resp, localReq.MaxCacheTime)

			if err != nil {

				localReq.Err = err
				localReq.ResponseChannel <- nil

			} else {
				response.CachedTime = time.Now().Unix()
				dispatch.cache.Cache(localReq,response)

				localReq.Err = nil
				localReq.ResponseChannel <- response

			}

		}

		//Remove Request from Queue
		dispatch.inflight.Remove(localReq)

		dispatch.Log("Network request hsas completed for %v.",localReq.URL.String())
		dispatch.requestCompletedBroadcaster <- localReq.URL.String()
	
}

func (dispatch *Dispatcher) wait(){
	for{
		completedRequestURL := <-dispatch.requestCompletedBroadcaster
		for _,pendingRequest := range dispatch.queue{
			if pendingRequest.URL.String() == completedRequestURL {

				dispatch.Log("'%v' was waiting. will use cached response.",completedRequestURL)
				
				resp,_ := dispatch.cache.Find(pendingRequest)
				pendingRequest.ResponseChannel <- resp
			}
		}
	}
}

func (dispatch *Dispatcher) SetCachePredicate(predicate func(request *ghttp.Request,respone *ghttp.Response) bool){
	dispatch.cache.Predicate = predicate
}

func (dispatch *Dispatcher) SetMaxCacheTime(maxCacheTime time.Duration){
	dispatch.cache.MaxCacheTime = maxCacheTime
}

func (dispatch *Dispatcher) SetMaxCacheLength(maxCacheLength int) {
	dispatch.cache.MaxCacheLength = maxCacheLength
}

func (dispatch *Dispatcher) DefaultCachePredicate() func(request *ghttp.Request,respone *ghttp.Response) bool{
	return cache.Predicate
}

func RequestStrategy(dispatcher *Dispatcher, request *ghttp.Request) (*http.Response, error) {

	return dispatcher.Do(request.Request)
}


func Log(format string, a ...interface{}) {

	if len(a) > 0 {
		log.Println(fmt.Sprintf(format, a))
	} else {
		log.Println(format)
	}

}
