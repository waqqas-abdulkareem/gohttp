package cache

import(
	"bitbucket.org/waqqas-abdulkareem/gohttp/ghttp"
	"time"
	"fmt"
)

const DefaultMaxCacheTime time.Duration = time.Hour

const DefaultMaxCacheLength int = 100

type Cache struct{
	MaxCacheTime time.Duration
	MaxCacheLength int
	responses map[string]*ghttp.Response
	addResponse chan Add
	removeResponse chan Remove
	findResponse chan Find
	quit chan struct{}
	Predicate func(request *ghttp.Request,response *ghttp.Response) bool
}

type Add struct{
	Request *ghttp.Request
	Response *ghttp.Response
}

type Remove struct{
	Request *ghttp.Request
}

type Find struct{
	Request *ghttp.Request
	ResponseChannel chan *ghttp.Response
}

func New() *Cache{
	return &Cache{
		MaxCacheTime:DefaultMaxCacheTime,
		MaxCacheLength:DefaultMaxCacheLength,
		responses: make(map[string]*ghttp.Response),
		addResponse: make(chan Add,1),
		removeResponse:make(chan Remove,1),
		findResponse:make(chan Find,1),
		quit:make(chan struct{},1),
		Predicate:Predicate,
	}
}

func (cache *Cache) Run(){

	for{
		select{
		case in := <- cache.addResponse:

			cache.CleanUp()

			if cache.Predicate(in.Request,in.Response) {
				cache.responses[in.Request.URL.String()] = in.Response
			}

		case in := <- cache.removeResponse:

			cache.CleanUp()

			delete(cache.responses,in.Request.URL.String())

		case in := <- cache.findResponse:
			in.ResponseChannel <- cache.responses[in.Request.URL.String()]

		case <- cache.quit:
			return
		}
	}
}

func (cache *Cache) Find(request *ghttp.Request) (*ghttp.Response,bool){
	ret := make(chan *ghttp.Response,1)
	cache.findResponse <- Find{
		Request: request,
		ResponseChannel: ret,
	}

	resp := <-ret

	return resp,(resp != nil)
}

func (cache *Cache) Cache(request *ghttp.Request,response *ghttp.Response){
	cache.addResponse <- Add{
		Request: request,
		Response: response,
	}
}

func (cache *Cache) Uncache(request *ghttp.Request){
	cache.removeResponse <- Remove{
		Request: request,
	}
}

func (cache *Cache) Close(){
	cache.quit <- struct{}{}
}


func Predicate(request *ghttp.Request, response *ghttp.Response) bool{

	return response.StatusCode == 200 && !response.Expired()

}


func (cache *Cache) CleanUp() {

	timeLimitExceeded := func(resp *ghttp.Response) bool {
		return cache.MaxCacheTime.Nanoseconds() < (time.Now().Unix()-resp.CachedTime)
	}

	for reqUrl, resp := range cache.responses {

		expired := resp.Expired()
		timeLimitExceeded := timeLimitExceeded(resp)

		if expired || timeLimitExceeded {
			//not sure if one can remove item from map while iterating over it.
			fmt.Printf("Deleting response for %s. Expired: %s, Time Limit Exceeded: %s\n", reqUrl, expired, timeLimitExceeded)
			delete(cache.responses, reqUrl)
		}

		
	}

	//Cache Length TO DO

}