package main

import (
	"bitbucket.org/waqqas-abdulkareem/gohttp"
	"bitbucket.org/waqqas-abdulkareem/gohttp/ghttp"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

//------------------------- CONSTANTS -------------------------

const GRISBNServiceUrl = "https://www.goodreads.com/book/isbn"
const key string = "ISOVYkp58KaoaQXUtN1vAQ"
const secret string = "5yrd7hWCP2c8MgZmMt2O13HZ0khelV5e3wBFYBooZvg"

//------------------------- GLOBALS -------------------------

var dispatcher *gohttp.Dispatcher
var validBookRatingPattern = regexp.MustCompile(`((5|4|3|2|1|total):\d+(|)?)*`)

//------------------------- TYPES -------------------------

type Handler struct{}

type Author struct {
	XMLName xml.Name `xml:"author" json:"-"`
	Name    string   `xml:"name"`
}

type Book struct {
	XMLName             xml.Name `xml:"GoodreadsResponse" json:"-"`
	Title               string   `xml:"book>title"`
	Authors             []Author `xml:"book>authors>author"`
	RatingsDistribution string   `xml:"book>work>rating_dist" json:"-"`
	Ratings             Ratings
	ISBN                string `xml:"book>isbn13"`
}

type Ratings struct {
	FiveStars  int `json:"5"`
	FourStars  int `json:"4"`
	ThreeStars int `json:"3"`
	TwoStars   int `json:"2"`
	OneStar    int `json:"1"`
}

type Error struct {
	Message string
}

type Response struct {
	Code int
	Data interface{}
}

//------------------------- FUNCTIONS -------------------------

func (book *Book) ParseRatings() error {

	//fmt.Printf("%s\n", book.RatingsDistribution)

	if !validBookRatingPattern.MatchString(book.RatingsDistribution) {
		return errors.New("Error processing ratings.")
	}

	var ratings Ratings
	var err error

	distributions := strings.Split(book.RatingsDistribution, "|")
	//fmt.Printf("%v\n", distributions)

	for _, distribution := range distributions {
		parts := strings.Split(distribution, ":")
		//fmt.Printf("\t%v\n", parts)
		switch parts[0] {
		case "5":
			ratings.FiveStars, err = strconv.Atoi(parts[1])
			break
		case "4":
			ratings.FourStars, err = strconv.Atoi(parts[1])
			break
		case "3":
			ratings.ThreeStars, err = strconv.Atoi(parts[1])
			break
		case "2":
			ratings.TwoStars, err = strconv.Atoi(parts[1])
			break
		case "1":
			ratings.OneStar, err = strconv.Atoi(parts[1])
			break
		}
	}

	book.Ratings = ratings

	return err
}

func (self Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	isbn := r.FormValue("isbn")

	if len(isbn) == 0 {
		renderJson(w, http.StatusBadRequest, Error{Message: "No ISBN Provided"})
		return
	}

	params := url.Values{}
	params.Set("format", "xml")
	params.Set("key", key)
	params.Set("isbn", isbn)

	req, _ := ghttp.NewRequestWithParams("GET", GRISBNServiceUrl, params)
	req.MaxCacheTime = 24 * time.Hour

	fmt.Println("Sending Request.")
	dispatcher.RequestChannel <- &req

	fmt.Println("Waiting for response...")
	resp := <-req.ResponseChannel

	if req.Err != nil {

		renderJson(w, http.StatusInternalServerError, "Bad")
		return
	}

	book := &Book{}

	err := xml.Unmarshal(resp.Content, book)

	if err != nil {
		renderJson(w, http.StatusInternalServerError, Error{Message: err.Error()})
		return
	}

	err = book.ParseRatings()

	if err != nil {
		renderJson(w, http.StatusInternalServerError, Error{Message: err.Error()})
		return
	}

	if err != nil {
		renderJson(w, http.StatusInternalServerError, Error{Message: err.Error()})
		return
	}

	//fmt.Printf("---\n\n%s\n\n---",string(resp.Content))
	renderJson(w, http.StatusOK, book)

}

func renderJson(w http.ResponseWriter, httpCode int, data interface{}) {

	w.WriteHeader(httpCode)
	w.Header().Set("Content-Type", "application/json")

	resp := Response{Code: httpCode, Data: data}
	json, err := json.MarshalIndent(resp, "", "\t")

	if err != nil {
		fmt.Fprint(w, err.Error())
	}

	fmt.Fprint(w, string(json))

}

func init() {

	dispatcher = gohttp.NewDispatcher()
	dispatcher.SetMaxCacheLength(100)
	dispatcher.SetMaxCacheTime(24 * time.Hour)

	go dispatcher.Run()
}

func main() {

	var h Handler

	fmt.Println("Listening on 127.0.0.1:4000")
	err := http.ListenAndServe("localhost:4000", h)
	if err != nil {
		fmt.Println(err.Error())
	}
}

//----------------------------   TEST DATA  -----------------------------
//
//	Harry Potter and the Philosopher's Stone		9788478888566
//	Harry Potter and the Chamber of Secrets			9780747560722
//	Harry Potter and the Prisoner of Azkaban		9780747560777
//	Harry Potter and the Goblet of Fire				9780195799163	
//	Harry Potter and the Order of the Phoenix		9788498383621
//
//
//-----------------------------------------------------------------------
