package inflight

import(
	"bitbucket.org/waqqas-abdulkareem/gohttp/ghttp"
)

type Inflight struct{
	requests		map[string]*ghttp.Request
	addRequest		chan *ghttp.Request
	removeRequest	chan *ghttp.Request
	findRequest		chan Find
	quit			chan struct{}
}

type Find struct{
	Request *ghttp.Request
	ResultChannel chan bool
}

func New() *Inflight{
	return &Inflight{
		requests:		make(map[string]*ghttp.Request),
		addRequest:		make(chan *ghttp.Request,1),
		removeRequest:	make(chan *ghttp.Request,1),
		findRequest:	make(chan Find,1),
		quit:			make(chan struct{},1),
	}
}

func (inflight *Inflight) Run(){
	for{
		select{
		case req := <- inflight.addRequest:
			inflight.requests[req.URL.String()] = req

		case req := <-inflight.removeRequest:
			delete(inflight.requests,req.URL.String())

		case find := <-inflight.findRequest:
			_,found := inflight.requests[find.Request.URL.String()]
			find.ResultChannel <- found

		case <-inflight.quit:
			return
		}
	}
}

func (inflight *Inflight) Add(request *ghttp.Request){
	inflight.addRequest <- request
}

func (inflight *Inflight) Remove(request *ghttp.Request){
	inflight.removeRequest <- request
}

func (inflight *Inflight) Find(request *ghttp.Request) bool{
	ret := make(chan bool,1)
	inflight.findRequest <- Find{
		Request: request,
		ResultChannel: ret,
	}
	return <-ret
}

func (inflight *Inflight) Close(){
	inflight.quit <- struct{}{}
}
