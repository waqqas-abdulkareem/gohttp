# GoHTTP #

GoHttp is a basic request queue implementation.

If your web service sends a lot of concurrent HTTP requests, then GoHttp can help you manage them. 

## Features ##

* Each request has its own dedicated Response and ErrorChannel.
* Ensures that multiple matching requests are not sent to a third-party service at the same time. Instead, the later request waits for the first request to complete and then retrieves the response from the cache.
* Handles cache synchronization.
* Cache can be configured to set maximum cache time and maximum cache size.

## Example ##

In the example below, we're creating a RESTful web service that receives the ISBN number of a book and returns the book's name, author and other details. Details of the book are obtained using GoodRead's Web API.

The example uses GoHTTP to ensure that only one request for one book gets sent in one hour and details for subsequent matching requests are returned from the cache. 

```
#!go
package main

import (
	_"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"bitbucket.org/waqqas-abdulkareem/gohttp"
	"bitbucket.org/waqqas-abdulkareem/gohttp/ghttp"
	"time"
)

type Handler struct{}

//declare a reference to the Request Queue
var rq * gohttp.RequestQueue

func init() {

	dispatcher = gohttp.NewDispatcher()
	dispatcher.SetMaxCacheLength(100)
	dispatcher.SetMaxCacheTime(24 * time.Hour)

	go dispatcher.Run()
}

func (self Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	isbn := r.FormValue("isbn")

	if len(isbn) == 0 {
		http.Error(w, "No ISBN provided", http.StatusInternalServerError)
		return
	}

	params := url.Values{}
	params.Set("format", "xml")
	params.Set("key", key)
	params.Set("isbn", isbn)

	req, _ := ghttp.NewRequestWithParams("GET", "https://www.goodreads.com/book/isbn", params)
	req.MaxCacheTime = time.Hour

	rq.RequestChannel <- &req

	resp := <-req.ResponseChannel

	if req.Err != nil {

		http.Error(w, fmt.Sprintf("Request Failed: %s", req.Err.Error()), http.StatusInternalServerError)
		return
	}

	fmt.Println(string(resp.Content));

    //...Possibly unmarshall xml here

}

func main() {

	var h Handler
	fmt.Println("Listening on 127.0.0.1:4000")
	err := http.ListenAndServe("localhost:4000", h)
	if err != nil {
		fmt.Println(err.Error())
	}
}
```