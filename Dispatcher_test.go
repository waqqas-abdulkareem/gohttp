package gohttp

import (
	"fmt"
	"net/url"
	"strconv"
	"testing"
	"time"
)

var rq *RequestQueue

func TestSetUp(t *testing.T) {

	rq = NewRequestQueue()
	go rq.HandleRequestsAsync()

	fmt.Println("Request Queue is running....\n")
}

func TestResponseReceivedOnResponseChannel(t *testing.T) {

	req := newValidRequest()
	requestHelper(t, &req, func(t *testing.T, resp *Response) {

		fmt.Printf("PASS. Status Code: %v\n", resp.StatusCode)

	}, func(t *testing.T, err error) {

		t.Errorf("FAIL. Expected response on ResponseChannel. Got Error: %v\n", err.Error())

	})

}

func TestErrorReceivedOnErrorChannel(t *testing.T) {

	req := newInvalidRequest()
	requestHelper(t, &req, func(t *testing.T, resp *Response) {

		t.Errorf("FAIL.Expected error on ErrorChannel. Got Status: %v\n", resp.StatusCode)

	}, func(t *testing.T, err error) {

		fmt.Printf("PASS. TestErrorReceivedOnErrorChannel: %v\n", err.Error())

	})

}

func TestCheckCache(t *testing.T) {

	req := newValidRequest()
	req.MaxCacheTime = time.Minute * 2

	requestHelper(t, &req, func(t *testing.T, resp *Response) {

		_, actualResult := rq.cache[req.URL.String()]
		_, checkCacheResult := rq.checkCache(&req)

		if !actualResult {
			t.Errorf("FAIL. Expected response to be cached.\n")
			return
		}

		if checkCacheResult != actualResult {
			t.Errorf("FAIL. Expected checkCache to return %v. Got %v.\n", actualResult, checkCacheResult)
			return
		}

		fmt.Println("PASS")
		delete(rq.cache, req.URL.String())

	}, func(t *testing.T, err error) {

		t.Errorf("FAIL. Expected Request to suceed. Got error: %s\n", err.Error())

	})
}

func TestAddToCache(t *testing.T) {

	req := newValidRequest()
	req.MaxCacheTime = time.Minute * 2

	requestHelper(t, &req, func(t *testing.T, resp *Response) {

		_, cached := rq.cache[req.URL.String()]

		if !cached {
			t.Errorf("FAIL. Expected response to be cached.\n")
		}

		fmt.Println("PASS")
		delete(rq.cache, req.URL.String())

	}, func(t *testing.T, err error) {

		t.Errorf("FAIL. Expected Request to suceed. Got error: %s\n", err.Error())

	})

}

func TestManageCacheWithLengthExcess(t *testing.T) {

	rq.Config.MaxCacheLength = 2

	req := newValidRequest()

	requestHelper(t, &req, func(t *testing.T, resp *Response) {

		responses := copyResponses(resp, rq.Config.MaxCacheLength+3)

		for i, resp := range responses {

			url, _ := url.Parse("http://www.google.com/" + strconv.Itoa(i))
			req.URL = url

			resp.MaxCacheTime = 10 * time.Second

			rq.addToCache(&req, resp)

		}

		if len(rq.cache) != rq.Config.MaxCacheLength {
			t.Errorf("FAIL. Expected Cache Length: %d, Got: %d\n", rq.Config.MaxCacheLength, len(rq.cache))
		} else {
			fmt.Printf("PASS. Items: %d\n", len(rq.cache))
		}

	}, func(t *testing.T, err error) {

		t.Errorf("FAIL. Expected request to succeed. Got error: %s", err.Error())

	})

	rq.Config.MaxCacheLength = DefaultMaxCacheLength
}

//-----------------------HELPER METHODS--------------------

func newValidRequest() Request {

	params := url.Values{}
	params.Add("q", "London,uk")

	req, _ := NewRequestWithParams("GET", "http://api.openweathermap.org/data/2.5/weather", params)

	return req
}

func newInvalidRequest() Request {

	params := url.Values{}
	params.Add("q", "London,uk")

	req, _ := NewRequestWithParams("GET", "http://api", params)

	return req
}

func requestHelper(t *testing.T, req *Request, responseHandler func(t *testing.T, resp *Response), errorHandler func(t *testing.T, err error)) {

	fmt.Printf("> Executing Request: %v\n", req.URL.String())

	rq.RequestChannel <- req
	select {
	case resp := <-req.ResponseChannel:

		responseHandler(t, resp)

	case err := <-req.ErrorChannel:

		errorHandler(t, err)

	}

	fmt.Println()

}

func normalRequestQueueConfig() Config {
	return Config{
		MaxCacheTime:   DefaultMaxCacheTime,
		MaxCacheLength: DefaultMaxCacheLength,
	}
}

func copyResponses(resp *Response, copies int) []*Response {

	responses := make([]*Response, copies)

	for i := 0; i < copies; i++ {

		respCopy := *resp
		responses[i] = &respCopy

	}

	return responses
}
