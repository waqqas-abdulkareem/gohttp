package ghttp

import (
	"fmt"
	"net/http"
	"net/url"
	"time"
)

//Request extends http package's request by adding a ResponseChannel, an ErrorChannel and a MaxCacheTime.
type Request struct {
	*http.Request
	//Channel through which the response will be delivered to the client.
	//Use a select statement to receive the response either from the response channel or the error channel.
	//This channel has a buffer size of 1.
	ResponseChannel chan *Response
	//If an error occurrs while performing the request, details of the error will be sent through this channel.
	//Use a select statement to receive the response either from the response channel or the error channel.
	Err error
	//The maximum duration that the response will be cached. A response will only be cached if its HTTP status is OK.
	MaxCacheTime time.Duration
}

//Returns a new Request instance with given HTTP request method and request url.
//The returned instance of request has a response channel with a buffer size of 1.
func NewRequest(requestMethod string, requestUrl string) (Request, error) {

	_req, _err := http.NewRequest(requestMethod, requestUrl, nil)
	if _err != nil {
		return Request{}, _err
	}

	req := Request{
		Request:         _req,
		ResponseChannel: make(chan *Response, 1),
	}

	return req, nil
}

//Returns a new Request instance with given HTTP request method,  url and params.
//The returned instance of request has a response channel with a buffer size of 1.
func NewRequestWithParams(requestMethod string, requestUrl string, params url.Values) (Request, error) {

	if len(params) > 0 {
		requestUrl += "?"
	}

	url := fmt.Sprintf("%s%s", requestUrl, params.Encode())
	return NewRequest(requestMethod, url)
}
