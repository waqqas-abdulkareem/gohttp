package ghttp

import (
	"testing"
	"time"
)

func TestExpiredWhenNotExpired(t *testing.T) {
	r := Response{MaxCacheTime: time.Hour, CachedTime: time.Now().Unix()}

	if r.Expired() {
		t.Errorf("Expired() returns true when response has not expired.")
	}
}

func TestExpiredWhenExpired(t *testing.T) {
	r := Response{MaxCacheTime: time.Hour, CachedTime: time.Now().Unix() - (2 * time.Hour.Nanoseconds())}

	if !r.Expired() {
		t.Errorf("Expired() returns false when response has expired")
	}
}

func TestExpiredWhenMaxCacheTimeNotSet(t *testing.T) {
	r := Response{CachedTime: time.Now().Unix()}

	if !r.Expired() {
		t.Errorf("Expired() should always return true if MaxCacheTime is not set.")
	}
}

func TestExpiredWhenCachedTimeNotSet(t *testing.T) {
	r := Response{MaxCacheTime: 100}

	if !r.Expired() {
		t.Errorf("Expired should return true if CachedTime is not set.")
	}
}
