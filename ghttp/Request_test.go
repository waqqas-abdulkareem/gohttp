package ghttp

import (
	"net/url"
	"testing"
)

func TestNewRequestWithValidUrl(t *testing.T) {
	_, err := NewRequest("GET", "htp://www.google.com/?name=test")

	if err != nil {
		t.Errorf("New Request does returns error for valid URL.")
	}
}

func TestNewRequestWithInvalidUrl(t *testing.T) {
	_, err := NewRequest("GET", "http://www")

	if err != nil {
		t.Errorf("New Request does not return error for invalid URL.")
	}
}

func TestNewRequestWithParamsWithParams(t *testing.T) {

	params := url.Values{}
	params.Add("name", "test")
	params.Add("job", "developer")

	req, _ := NewRequestWithParams("GET", "http://www.google.com", params)
	expected := "http://www.google.com?job=developer&name=test"

	if req.URL.String() != expected {
		t.Errorf("NewRequestWithParams does not add params as expected.\n\tExpected: %s,\n\tGot: %s", expected, req.URL.String())
	}
}

func TestNewRequestDefaultMaxCacheTimeIsZero(t *testing.T) {

	req, _ := NewRequest("GET", "http://www.google.com")
	if req.MaxCacheTime != 0 {
		t.Errorf("NewRequest.MaxCacheTime is not zero by default")
	}
}
