package ghttp

import (
	"io/ioutil"
	"net/http"
	"time"
)

//gohttp.Response extends go's http.Response by adding MaxCacheTime, Cached Time and Content.
type Response struct {

	//Http.Response received for a request
	*http.Response

	//Maximum amount of time that the response should be kept in the cache.
	MaxCacheTime time.Duration

	//Time when the response was added to cache
	CachedTime int64

	//The content of the response.
	//Note that when a gohttp.Response instance is created via NewResponse,
	//the body of the http.Response is read and saved in the Content field.
	//This means that the body field can no longer be used in gohttp.Response.
	Content []byte
}

//Creates a new instance of gohttp.Response with given http.Response.
//The response will be cached for the given maxCacheTime duration.
//Note that this function reads the body of http.Respose and saves the data in the Content field.
func NewResponse(resp *http.Response, maxCacheTime time.Duration) (*Response, error) {

	defer resp.Body.Close()
	contents, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	return &Response{Response: resp, MaxCacheTime: maxCacheTime, Content: contents}, nil
}

//Returns true if this response has expired its stay in the cache.
func (self *Response) Expired() bool {
	return time.Now().Unix()-self.CachedTime >= self.MaxCacheTime.Nanoseconds()
}
